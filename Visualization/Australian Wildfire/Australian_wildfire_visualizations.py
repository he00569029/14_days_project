#import packages
import numpy as np 
import pandas as pd 
import matplotlib.pyplot as plt
import folium as folium
from folium.plugins import HeatMap
from folium import plugins

# Load Data
path = 'D:/VScode_directory/14_days_project/Visualization/Australian Wildfire/dataset/'
fire_archive_M6_96619 = pd.read_csv(path+'fire_archive_M6_96619.csv')
# fire_archive_V1_96617 = pd.read_csv(path+'fire_archive_V1_96617.csv')
# fire_nrt_M6_96619 = pd.read_csv(path+'fire_nrt_M6_96619.csv')
# fire_nrt_V1_96617 = pd.read_csv(path+'fire_nrt_V1_96617.csv')

# 假設先以archive_M6_96619 做為資料視覺化的題目
# 構想是  畫出澳洲的地圖  因為沒有城市名稱  所以用經緯度做替代  然後告訴我每個經緯度的frp (輻射值)
# 由於plotly無法畫出美國以外的地圖  所以採用basemap 參考網址如下
# https://jakevdp.github.io/PythonDataScienceHandbook/04.13-geographic-data-with-basemap.html


# setup up map
fmap = folium.Map( location=[ -15.706500 , 136.738500 ] , zoom_start=12)
fmap.save('map.html')

# the frp value where the cood
data = fire_archive_M6_96619.loc[:, ['latitude','longitude','frp'] ]

# add this information to map , so do the heatmap
HeatMap(data).add_to(fmap)
fmap.save('map.html')


# Plot Time Series Heatmap
data_time_series = fire_archive_M6_96619.loc[:, ['latitude','longitude','acq_date','frp'] ]
# drop duplicates
data_time_series.drop_duplicates(subset = 'acq_date' ,inplace= True)
# set_index
data_time_series.set_index('acq_date' , inplace=True)
hm = plugins.HeatMapWithTime(data_time_series , auto_play=True , max_opacity=0.8 )
hm.add_to(fmap)
fmap.save('time_map.html')



