#import packages
import numpy as np 
import pandas as pd 
import plotly as py
import plotly.express as px
import plotly.graph_objs as go
import plotly.io as pio
from plotly.subplots import make_subplots
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)

# 欄位描述- covid_19_data.csv
# Sno 序列號碼
# Date 觀察到的日期  MM/DD/YY
# Province/State 觀察到的省或洲
# Country 觀察到的城市
# Last Update 該城市或洲的更新時間
# Confirmed 截至該日的確診病例
# Deaths 截止該日的死亡病例
# Recovered 截止該日的康復病例

# 不使用 2019_ncov_data.csv

##############  核心思考  #########
# 我們要做資料視覺化  因為是要用地圖方式的顯示  所以應該要用  地區/城市、確診人數、日期 來進行顯示
##################################

# Load Data
path="D:/VScode_directory/14_days_project/Visualization/Coronavirus/dataset/"
df = pd.read_csv(path+"covid_19_data.csv")

# Rename columns
df = df.rename(columns={'Country/Region':'Country'})
df = df.rename(columns={'ObservationDate':'Date'})


# Manipulate Dataframe
# 因為我們要看的是地圖  所以會以時間跟地區為主要顯示  所以才以Country Date兩個欄位做群組
df_countries = df.groupby(['Country', 'Date']).sum()  #以Country Date進行分組  並統計  因此用到sum
df_countries.reset_index(inplace=True) #重新編index
df_countries.sort_values('Date' , ascending=False , inplace=True) #使用日期做排序 遞減排序
df_countries.drop_duplicates(subset = ['Country'], inplace=True)  #針對country欄位 進行剔除重複的row
df_countries = df_countries[df_countries['Confirmed']>0]  #選取有確診的


# Create the Choropleth   地圖資料視覺化參數
fig = go.Figure(data=go.Choropleth(
    locations = df_countries['Country'],
    locationmode = 'country names',
    z = df_countries['Confirmed'],
    colorscale = 'Reds',
    marker_line_color = 'black',
    marker_line_width = 0.5,
))

fig.update_layout(
    title_text = 'Confirmed Cases as of March 28, 2020',
    title_x = 0.5,
    geo=dict(
        showframe = False,
        showcoastlines = False,
        projection_type = 'equirectangular'
    )
)

# 顯示 地圖
pio.show(fig)



### Animated choropleth map

# Manipulating the original dataframe
df_countrydate = df[df['Confirmed']>0]  #選取確診人數>0的資料
df_countrydate = df_countrydate.groupby(['Date','Country']).sum().reset_index()
df_countrydate

# Creating the visualization
fig = px.choropleth(df_countrydate, 
                    locations="Country", # 以dataframe的欄位當作主要輸出的東西
                    locationmode = "country names",  # 會與locations參數做連結並反映到地圖上 有ISO-3  USA-states  country names
                    color="Confirmed", # 以甚麼欄位來做顏色上的區分
                    hover_name="Country", #dataframe的欄位  主要是用粗線條做區分欄位裡面的資料  畫出那個界線
                    animation_frame="Date"  #以dataframe的date欄位來做影格播放
                   )

fig.update_layout(
    title_text = 'Global Spread of Coronavirus',
    title_x = 0.5,
    geo=dict(
        showframe = False,
        showcoastlines = False,
    ))
    
fig.show()


