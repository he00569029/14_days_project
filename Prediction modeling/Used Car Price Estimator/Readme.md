# Used Car Price Estimator

## Topic Description

Craigslist is the world's largest collection of used vehicles for sale, yet it's very difficult to collect all of them in the same place. I built a scraper for a school project and expanded upon it later to create this dataset which includes every used vehicle entry within the United States on Craigslist.

This data is scraped every few months, it contains most all relevant information that Craigslist provides on car sales including columns like price, condition, manufacturer, latitude/longitude, and 18 other categories. For ML projects, consider feature engineering on location columns such as long/lat. For previous listings, check older versions of the dataset.

## Goal

- Predict vehicles price for sale.
  
## Data Description

- Vehicles.csv
  - id
  - url : listing URL
  - region : craigslist region
  - region_url : region URL
  - price : entry price
  - year
  - manfacturer : manufacturer of vehicle
  - model : manufacturer of vehicle
  - condition : condition of vehicle
  - cylinders : number of cylinders
  - fuel : fuel type
  - odometer : miles traveled by vehicle
  - title_status : title status of vehicle
  - transmission : transmission of vehicle
  - vin : vehicle identification number
  - drive : type of drive
  - size
  - type : generic type of vehicle
  - paint_color
  - image_url
  - description : listed description of vehicle
  - county : useless column left in by mistake
  - state : state of listing
  - lat : latitude of listing
  - long : longitude of listing

## Reference

- [Kaggle link](https://www.kaggle.com/austinreese/craigslist-carstrucks-data)

- [Craigslist Filter](https://github.com/AustinReese1998/craigslistFilter)
