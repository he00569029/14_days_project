# import packages
import load_data as load_data
import preprocessing as pre
import Feature_selection as fs
import Model as model
import Validation as validation
import Predict as predict
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

# Load data
PATH='D:/VScode_directory/14_days_project/Prediction modeling/Used Car Price Estimator/Dataset/'
vehicle = load_data.load_data(PATH,'vehicles')

# Preprocessing - Drop constant_column
pre.drop_constant_column(vehicle)
# Preprocessing - Drop unnecessary feature
drop_feature = ['url','image_url','vin','region_url','description']
vehicle.drop(drop_feature,axis=1,inplace=True)
# Preprocessing - Drop row of missing value
vehicle.dropna(inplace=True)
# Preprocessing - Label encoding
labelencoder = LabelEncoder()
discrete_column = vehicle.select_dtypes('object').columns
data = vehicle
for col in discrete_column:
    data[col] = labelencoder.fit_transform(data[col])
# Preprocessing - One hot encoding
a = pd.get_dummies(vehicle)


# Train Validation Test set split
target_column=['price']
train, test_data, y_train , y_test_data = train_test_split(data.drop(target_column,axis=1) , data[target_column])
train_data , validation_data , y_train_data , y_validation_data = train_test_split(train , y_train)


# Modeling - xgb
model_method = model.model_params(train_data , y_train_data)
model_method.xgb_regression_model(validation_data , y_validation_data)
# Modeling - lightgbm
model_method.gbm_regression_model(validation_data , y_validation_data)
# Modeling - random forest
model_method.random_forest_regression_model()


# Validation

