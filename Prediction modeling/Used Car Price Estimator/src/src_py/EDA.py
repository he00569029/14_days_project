# import packages
import load_data as load_data
import pandas as pd
import seaborn as sns;sns.set()
import matplotlib.pyplot as plt
import plotly.express as px
import missingno as msno

# Load Data
PATH='D:/VScode_directory/14_days_project/Prediction modeling/Used Car Price Estimator/Dataset/'
vehicle = load_data.load_data(PATH,'vehicles')

# Data describe
vehicle.describe(include='all')
vehicle.info()
vehicle.shape

# Check column of missing value
print('These columns have missing value\n',list(vehicle.columns[vehicle.isnull().any()]) )
msno.matrix(vehicle)
plt.show()


# Check constant feature
constant_feature=[]
for col in vehicle.columns:
    if (len(vehicle[col].value_counts()) <= 1):
        print(col,'is constant value')
        constant_feature.append(col)
# Drop constant feature
vehicle.drop(constant_feature,axis=1,inplace=True)

# Drop unnecessary feature
drop_feature = ['url','image_url','vin','region_url','description']
vehicle.drop(drop_feature,axis=1,inplace=True)




# Target output distribution
sns.kdeplot(vehicle['price'] , shade=True)
plt.show()

sns.boxplot(x='price' , data=vehicle)
plt.show()

# pairplot
numeric_column = vehicle.select_dtypes(include=['float64','int64']).columns
sns.pairplot(data=vehicle[numeric_column])
plt.tight_layout()
plt.show()

# Corr
vehicle.corr()
vehicle.corr(method='spearman')
# Heatmap for pearson correlation
sns.heatmap(vehicle.corr())
plt.tight_layout()
plt.show()
# Heatmap for sepearman correlation
sns.heatmap( vehicle.corr(method='spearman'))
plt.tight_layout()
plt.show()

# price condition catplot
sns.catplot(x='condition' , y='price' , data=vehicle)
plt.show()
# manufacturer vs condition catplot
sns.catplot(y='manufacturer' , x='price' , data=vehicle ,orient='h')
plt.show()
# type vs price catplot
sns.catplot(y='type' , x='price' , data=vehicle ,orient='h')
plt.show()
# type countplot
sns.countplot(x='type' , data=vehicle)
plt.show()
# type vs condition countplot
sns.countplot(x='type' , hue='condition' , data=vehicle)
plt.show()


# odometer distribution
sns.kdeplot(data=vehicle['odometer'] , shade=True)
plt.show()
sns.boxplot(x='odometer' , data=vehicle)
plt.show()

# odometer vs condition catplot
sns.boxplot(x='condition' , y='odometer' , data=vehicle)
plt.show()
# odometer vs manufacturer
sns.catplot(x='odometer' , y='manufacturer', data=vehicle,orient='h')
plt.show()
# odomter vs type
sns.boxplot(x='odometer' , y='type' , data=vehicle , orient='h')
plt.show()


