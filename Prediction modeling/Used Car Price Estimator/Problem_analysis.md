# Problem Analysis

## Workflow Step

1. Explore Data Analysis
2. Data Preprocessing
3. Feature Selection
4. Modeling
5. Predict and Validation
6. Tune

### Explore Data Analysis

- Know what to achieve in this project.
- Understand dataset. (Descriptive statistics, Data structure etc.)
- Visulization data.
- What is the correlation between Input and Output?

### Data Preprocessing

- 10 Folds Cross Validation
- Constant Feature
- Missing Value
- Outlier Detection

### Feature Selection

- Select K Best

### Modeling

- Random Forest
- Xgboost
- Lightgbm

### Predict and Validation

- Predict validation data
- Loss Function

### Tune

- GridSearch
