#import packages
from sklearn.metrics import accuracy_score
import math as math

class val_params():
    def __init__(self, model_1 , model_2 , model_3 , val_data , y_val_data , val_use_col):
        self.model_1 = model_1
        self.model_2 = model_2
        self.model_3 = model_3
        self.val_data = val_data
        self.y_val_data = y_val_data 
        self.val_use_col = val_use_col

    # Classification - acc
    def class_acc(self):
        val_pred_xgb = self.model_1.predict(self.val_data[self.val_use_col])
        val_pred_gbm = self.model_2.predict(self.val_data[self.val_use_col])
        val_pred_rf = self.model_3.predict(self.val_data[self.val_use_col])
        
        model_accuracy={'xgb_acc':round(accuracy_score(self.y_val_data,val_pred_xgb)*100 , 2) ,
        'gbm_acc':round(accuracy_score(self.y_val_data,val_pred_gbm)*100 , 2),
        'rf_acc':round(accuracy_score(self.y_val_data,val_pred_rf)*100 , 2)}

        return(model_accuracy)
    # Regression - rmse
    def reg_rmse(self):
        val_pred_xgb = self.model_1.predict( self.val_data )
        val_pred_gbm = self.model_2.predict( self.val_data )
        val_pred_rf = self.model_3.predict( self.val_data )

        rmse_xgb = math.sqrt( sum((self.y_val_data - val_pred_xgb)**2)  /self.val_data.shape[0] )
        rmse_gbm = math.sqrt( sum((self.y_val_data - val_pred_gbm)**2)/self.val_data.shape[0] )
        rmse_rf = math.sqrt( sum((self.y_val_data - val_pred_rf)**2)/self.val_data.shape[0] )
        
        model_rmse={'xgb_rmse':rmse_xgb,
        'gbm_rmse':rmse_gbm,
        'rf_rmse':rmse_rf}

        return(model_rmse)

    def reg_mse(self):
        val_pred_xgb = self.model_1.predict( self.val_data )
        val_pred_gbm = self.model_2.predict( self.val_data )
        val_pred_rf = self.model_3.predict( self.val_data )

        mse_xgb = sum((self.y_val_data - val_pred_xgb)**2)
        mse_gbm = sum((self.y_val_data - val_pred_gbm)**2)
        mse_rf = sum((self.y_val_data - val_pred_rf)**2)

        model_mse={'xgb_rmse':mse_xgb,
        'gbm_rmse':mse_gbm,
        'rf_rmse':mse_rf}

        return(model_mse)

    def reg_mae(self):
        val_pred_xgb = self.model_1.predict( self.val_data )
        val_pred_gbm = self.model_2.predict( self.val_data )
        val_pred_rf = self.model_3.predict( self.val_data )

        mae_xgb = sum(abs(self.y_val_data - val_pred_xgb) ) / self.val_data.shape[0]
        mae_gbm = sum(abs(self.y_val_data - val_pred_gbm) ) / self.val_data.shape[0]
        mae_rf = sum(abs(self.y_val_data - val_pred_rf) ) / self.val_data.shape[0]

        model_mae={'xgb_rmse':mae_xgb,
        'gbm_rmse':mae_gbm,
        'rf_rmse':mae_rf}

        return(model_mae)
