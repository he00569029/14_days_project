# import packages
import xgboost as xgb
import lightgbm as gbm
from sklearn.ensemble import RandomForestClassifier , RandomForestRegressor

class model_params():
    def __init__(self , train_data , y_train_data):
        self.train_data = train_data
        self.y_train_data = y_train_data

    # xgboost for scikit-learn API
    def xgb_model(self):
        model = xgb.XGBClassifier(n_estimators=1000)
        train_model = model.fit(self.train_data, self.y_train_data , verbose=True)
        return(train_model)

    # Lightgbm
    def gbm_model(self):
        gbm_model = gbm.LGBMClassifier(n_estimators=1000)
        train_model = gbm_model.fit( self.train_data , self.y_train_data)
        return(train_model)

    # Random Forest
    def random_forest_model(self):
        rf_model = RandomForestClassifier(n_estimators=10)
        train_model = rf_model.fit( self.train_data , self.y_train_data)
        return(train_model)
    
    def xgb_regression_model(self,eval_data,y_eval_data):
        xgb_reg_model = xgb.XGBRegressor(n_estimators = 10000, subsample=0.2 ,n_jobs=8)
        train_model = xgb_reg_model.fit( self.train_data , self.y_train_data , eval_set=[(eval_data,y_eval_data)] , early_stopping_rounds=500 , verbose=True)
        return(train_model)

    def gbm_regression_model(self,eval_data,y_eval_data):
        gbm_reg_model = gbm.LGBMRegressor(n_estimators=10000 , n_jobs=8)
        train_model = gbm_reg_model.fit(self.train_data , self.y_train_data , eval_set=[(eval_data,y_eval_data)] , early_stopping_rounds=500 , verbose=True)
        return(train_model)

    def random_forest_regression_model(self):
        rf_regression_model = RandomForestRegressor( n_estimators=1000 , n_jobs=8 , verbose=True)
        train_model = rf_regression_model.fit(self.train_data , self.y_train_data)
        return(train_model)
        
