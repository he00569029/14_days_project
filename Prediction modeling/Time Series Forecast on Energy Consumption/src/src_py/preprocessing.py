import pandas as pd

def drop_constant_column(data):
    constant_column=[]
    for column in data.columns:
        if len(data[column].value_counts())==1 :
            constant_column.append(column)
    return(data.drop(constant_column, axis=1 ,inplace=True))


def deal_missing_column(data):
    missing_column = data.columns[data.isnull().any()==True]
    for column_name in missing_column:
        if data[column_name].dtype == 'int64':
            data[column_name].fillna(data[column_name].mean(), inplace=True)

class transform():
    def __init__(self,data):
        self.trans_data = data
    
    def transform_datetime(self):
        self.trans_data['Datetime'] = pd.to_datetime(self.trans_data['Datetime'])
        self.trans_data['year'] = self.trans_data['Datetime'].dt.year
        self.trans_data['month'] = self.trans_data['Datetime'].dt.month
        self.trans_data['day'] = self.trans_data['Datetime'].dt.day
        self.trans_data['hour'] = self.trans_data['Datetime'].dt.hour
        self.trans_data['minute'] = self.trans_data['Datetime'].dt.minute
        self.trans_data['second'] = self.trans_data['Datetime'].dt.second
        return(self.trans_data)




    
