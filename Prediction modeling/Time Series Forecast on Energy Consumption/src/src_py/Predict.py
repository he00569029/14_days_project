#import packages
import pandas as pd
import math as math
from sklearn.metrics import accuracy_score

class predict_params():

    def __init__(self,test_data,y_test_data ):
        self.test_data = test_data
        self.y_test_data = y_test_data
    
    def class_acc(self, *args):
        """*args is which model you used"""
        for arg in args:
            pred = arg.predict(self.test_data)
        result = round(accuracy_score(self.y_val_data,pred)*100 , 2)
        return(result)


    def reg_rmse(self, *args ):
        """ *args is which model you used"""
        for arg in args:
            pred = arg.predict(self.test_data)
        result = math.sqrt(sum((self.y_test_data - pred)**2) / self.test_data.shape[0])
        return(result)

    def reg_mae(self, *args):
        """ *args is which model you used"""
        for arg in args:
            pred = arg.predict(self.test_data)
        result = sum(abs(self.y_test_data - pred)) / self.test_data.shape[0]
        return(result)
    
    def reg_mse(self, *args):
        """ *args is which model you used"""
        for arg in args:
            pred = arg.predict(self.test_data)
        result = sum((self.y_test_data - pred)**2)
        return(result)