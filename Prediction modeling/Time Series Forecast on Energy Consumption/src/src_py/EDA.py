# import packages
import os
import pandas as pd
import numpy as np
import load_data as load
import preprocessing as pre
import seaborn as sns;sns.set()
import matplotlib.pyplot as plt


PATH = 'D:/VScode_directory/14_days_project/Prediction modeling/Time Series Forecast on Energy Consumption/Dataset/'

# Load data 
file_name = os.listdir(PATH)
data_path=[]
for name in file_name:
    data_path.append(PATH+name)
data = pd.read_csv(data_path[file_name.index('NI_hourly.csv')])  # Load NI_hourly.csv

# Data Description
data.head()
data.info()
data.describe(include='all')

# Check column of missing value
data.isnull().any()
# Check row of missing value
data.loc[data.isnull().any(axis=1),:]

# Transform datatime
data['Datetime'] = pd.to_datetime(data['Datetime'])
data['year'] = data['Datetime'].dt.year
data['month'] = data['Datetime'].dt.month
data['day'] = data['Datetime'].dt.day
data['hour'] = data['Datetime'].dt.hour
data['minute'] = data['Datetime'].dt.minute
data['second'] = data['Datetime'].dt.second

# Data describe
data.describe()
# Drop constant column
pre.drop_constant_column(data)



# Distplot for NI_MW value
sns.distplot(data['NI_MW'])
plt.show()
# Relplot for all datetime
sns.relplot(x='Datetime',y='NI_MW' , kind='line' , ci=None , data = data)
plt.tight_layout()
plt.show()


# groupby year and month and calculation mean(NI_MW)
year_month = data.groupby(['year','month']).mean()
year_month['year_month'] = year_month.index.get_level_values('year').astype('str') +'-' + year_month.index.get_level_values('month').astype('str')
year_month['year'] = year_month.index.get_level_values('year').astype('str')
year_month['month'] = year_month.index.get_level_values('month')
# Relplot for year_month
fig = sns.relplot(x='year_month',y='NI_MW' , kind='line' , ci=None , data = year_month)
plt.xticks(year_month['year_month'],year_month['year_month'], rotation='vertical')
plt.show()
# Replot NI_MW of month  of Individual years 
sns.relplot(x='month',y='NI_MW' ,col='year', kind='line'  , data = year_month)
plt.show()


# groupby month and calculation mean(NI_MW)
month = data.groupby('month').mean()
month['datetime_month'] = month.index
# Relplot for months of all years
sns.relplot(x='datetime_month',y='NI_MW' , kind='line' , ci=None , data = month)
plt.show()


# groupby day and calculation mean(NI_MW)
day = data.groupby('day').mean()
day['datetime_day'] = day.index
# Relplot for days of all years
sns.relplot(x='datetime_day',y='NI_MW' , kind='line' , ci=None , data = day)
plt.show()


# groupby hour and calculation mean(NI_MW)
hour = data.groupby('hour').mean()
hour['datetime_hour'] = hour.index
# Relplot for hour of all years
sns.relplot(x='datetime_hour',y='NI_MW' , kind='line' , ci=None , data = hour ,col_order=['1','2','3','4','5','6','7','8','9','10','11','12'])
plt.show()


