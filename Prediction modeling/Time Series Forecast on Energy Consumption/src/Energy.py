# import packages
import os
import pandas as pd
import numpy as np
import preprocessing as pre
import Validation as validation
import Predict as pred
import Model as model
from sklearn.model_selection import train_test_split

PATH = 'D:/VScode_directory/14_days_project/Prediction modeling/Time Series Forecast on Energy Consumption/Dataset/'
# Load data 
file_name = os.listdir(PATH)
data_path=[]
for name in file_name:
    data_path.append(PATH+name)
data = pd.read_csv(data_path[file_name.index('NI_hourly.csv')])  # Load NI_hourly.csv



# Data Description
data.head()
data.info()
data.describe(include='all')

# Check column of missing value
data.columns[data.isnull().any()  == True]
data.loc[data.isnull().any(axis=1) == True,:]
# Data preprocessing - transform datetime and drop datetime
transform_method = pre.transform(data)
new_data = transform_method.transform_datetime()
new_data.drop('Datetime',axis=1,inplace=True)
# Data preprocessing - drop constant feature
pre.drop_constant_column(new_data)


# Train Validation Test split
# Because data of last year is too less, so we use recommand ratio to split data 
train , test_data , y_train , y_test_data = train_test_split(new_data.drop('NI_MW',axis=1) , new_data['NI_MW'], test_size=0.20)
train_data , validation_data , y_train_data , y_validation_data = train_test_split(train , y_train)


# Model - xgb.xgbregression
train_data , eval_data , y_train_data , y_eval_data = train_test_split(train_data , y_train_data , test_size=0.20)
model_method = model.model_params(train_data, y_train_data)
xgb_model = model_method.xgb_regression_model(eval_data,y_eval_data)
# Model - gbm.lgbmregression
gbm_model = model_method.gbm_regression_model(eval_data,y_eval_data)
# Mode - RandomForestRegressor
rf_model = model_method.random_forest_regression_model()

# Validation
validation_method = validation.val_params(xgb_model,gbm_model,rf_model,val_data=validation_data, y_val_data = y_validation_data ,val_use_col=validation_data.columns)
model_rmse = validation_method.reg_rmse()


# Predict test_data
pred_method = pred.predict_params(test_data , y_test_data)
result_rmse = pred_method.reg_rmse(gbm_model)
print('Test data rmse value is:',result_rmse )

