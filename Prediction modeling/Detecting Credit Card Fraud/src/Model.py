# import packages
import xgboost as xgb
import lightgbm as gbm
from sklearn.ensemble import RandomForestClassifier

class model_params():
    def __init__(self , train_data , y_train_data):
        self.train_data = train_data
        self.y_train_data = y_train_data

    # xgboost for scikit-learn API
    def xgb_model(self):
        model = xgb.XGBClassifier(n_estimators=1000)
        train_model = model.fit(self.train_data, self.y_train_data , verbose=True)
        return(train_model)

    # Lightgbm
    def gbm_model(self):
        gbm_model = gbm.LGBMClassifier(n_estimators=1000)
        train_model = gbm_model.fit( self.train_data , self.y_train_data)
        return(train_model)

    # Random Forest
    def random_forest_model(self):
        rf_model = RandomForestClassifier(n_estimators=10)
        train_model = rf_model.fit( self.train_data , self.y_train_data)
        return(train_model)