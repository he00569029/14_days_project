#import packages
from sklearn.metrics import accuracy_score

class val_params():
    def __init__(self, model_1 , model_2 , model_3 , val_data , y_val_data , val_use_col):
        self.model_1 = model_1
        self.model_2 = model_2
        self.model_3 = model_3
        self.val_data = val_data
        self.y_val_data = y_val_data 
        self.val_use_col = val_use_col

    # Validation model
    def validation_model(self):
        val_pred_xgb = self.model_1.predict(self.val_data[self.val_use_col])
        val_pred_gbm = self.model_2.predict(self.val_data[self.val_use_col])
        val_pred_rf = self.model_3.predict(self.val_data[self.val_use_col])
        
        model_accuracy={'xgb_acc':round(accuracy_score(self.y_val_data,val_pred_xgb)*100 , 2) ,
        'gbm_acc':round(accuracy_score(self.y_val_data,val_pred_gbm)*100 , 2),
        'rf_acc':round(accuracy_score(self.y_val_data,val_pred_rf)*100 , 2)}

        return(model_accuracy)