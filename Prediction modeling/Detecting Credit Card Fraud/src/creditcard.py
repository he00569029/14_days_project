# import packages
import pandas as pd
import numpy as np
import sys
import random as random
import load_data as load
import Feature_selection as fs
import preprocessing as pre
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import SelectKBest, chi2, f_classif , SelectFromModel
from sklearn.linear_model import  Lasso, LogisticRegression
import Model as model
import xgboost as xgb
import lightgbm as gbm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
import Validation as validation


# Load data
PATH='D:/VScode_directory/14_days_project/Prediction modeling/Detecting Credit Card Fraud/Dataset/'
creditcard = load.load_data(PATH, 'creditcard' )


# Data description
creditcard.head()
creditcard.describe(include='all')
creditcard.info()
# Check missing value column
creditcard.isnull().any()
# Check constan column
pre.drop_constant_column(creditcard)
# Target column frequency
pd.crosstab(index=creditcard['Class'] , columns='count')


# Train Validation Test split
train , test_data , y_train , y_test_data = train_test_split(creditcard.iloc[:,:-1],creditcard['Class'] , test_size=0.2)
train_data , val_data , y_train_data , y_val_data = train_test_split(train , y_train , test_size=0.2)

# Feature Selection - Select K Best
fs_method = fs.FS_params(train_data,y_train_data)
k_best_data = fs_method.fs_select_k_best(k_col=20)
k_best_val_data = val_data[k_best_data['k_best_column']]
# Feature Selection - L1 regularisation(Lasso)
fs_method = fs.FS_params(k_best_data['k_best_train_data'] , y_train_data)
fs_data = fs_method.fs_lasso()
print("Lasso feature is",fs_data['fs_feature'])
# Feature Selection - L2 regularisation(Rigde)
fs_method = fs.FS_params(k_best_data['k_best_train_data'] , y_train_data)
fs_data = fs_method.fs_ridge()
print("Rigde feature is",fs_data['fs_feature'])
# In this case, we decided to use L2 regularisation

# xgboost for scikit-learn API
model_method = model.model_params( fs_data['fs_train_data'] , y_train_data)
first_model_xgb = model_method.xgb_model()
# Lightgbm
second_model_gbm = model_method.gbm_model()
# Random Forest
third_model_rf = model_method.random_forest_model()


# Validation model with validation dataset 
validation_method = validation.val_params(first_model_xgb,second_model_gbm,third_model_rf, 
k_best_val_data,  y_val_data,  fs_data['fs_feature'] )
model_accuracy = validation_method.class_acc()
print('Xgboost Accuracy is:', model_accuracy['xgb_acc'] ,'%\n' ,
'Lightgbm Accuracy is:' , model_accuracy['gbm_acc'] , '%\n' ,
'Random Forest Accuracy is:', model_accuracy['rf_acc'] , '%\n')
