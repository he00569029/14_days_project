# import packages
import pandas as pd
import load_data as load
import preprocessing as pre
import seaborn as sns;sns.set()
import matplotlib.pyplot as plt



# Load data
PATH='C:/Users/he00569029/Desktop/vscode_dir/14_days_project/Prediction modeling/Detecting Credit Card Fraud/Dataset/'
creditcard = load.load_data(PATH, 'creditcard' )


# Data description
creditcard.head()
creditcard.describe(include='all')
creditcard.info()
# Check missing value column
creditcard.isnull().any()
# Target column frequency
pd.crosstab(index=creditcard['Class'] , columns='count')



# Heatmap for pearson correlation
fig = plt.figure(figsize=(18,12))
sns.heatmap(creditcard.corr(),annot=True,fmt='.2f' , xticklabels=creditcard.columns , yticklabels=creditcard.columns)
plt.tight_layout()
plt.subplots_adjust(top=0.99,right=0.99,left=0.05)
plt.show()



# Heatmap for spearman correlation
plt.figure(figsize=(18,12))
sns.heatmap(creditcard.corr(method='spearman'),annot=True,fmt='.2f' , xticklabels=creditcard.columns , yticklabels=creditcard.columns)
plt.tight_layout()
plt.subplots_adjust(top=0.99,right=0.99,left=0.05)
plt.show()
# Following set are greater than 0.2 of spearman correlation coefficient.
# 1 & 10
# 2 & 5 , 2 & 7
# 5 & 7 
# 6 & 8 
# 20 & 28
# 27 & 28


# pairplot
use_col=creditcard.columns[[1,2,5,6,7,8,20,27,28]]
# plot_kws=dict(scatter_kws=dict(s=2))
sns.pairplot(creditcard[use_col],diag_kind='kde',kind='reg' )
plt.tight_layout()
plt.show()

# distplot
fig , axes = plt.subplots(3,3)
sns.distplot(creditcard[creditcard.columns[1]] , ax=axes[0,0])
sns.distplot(creditcard[creditcard.columns[2]] , ax=axes[0,1])
sns.distplot(creditcard[creditcard.columns[5]] , ax=axes[0,2])
sns.distplot(creditcard[creditcard.columns[6]] , ax=axes[1,0])
sns.distplot(creditcard[creditcard.columns[7]] , ax=axes[1,1])
sns.distplot(creditcard[creditcard.columns[8]] , ax=axes[1,2])
sns.distplot(creditcard[creditcard.columns[20]] , ax=axes[2,0])
sns.distplot(creditcard[creditcard.columns[27]] , ax=axes[2,1])
sns.distplot(creditcard[creditcard.columns[28]] , ax=axes[2,2])
plt.tight_layout()
plt.show()

