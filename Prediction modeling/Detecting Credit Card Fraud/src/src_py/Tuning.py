# import package
import xgboost as xgb

def xgb_cv(train_data,y_train_data):
    # xgboost for learning API
    dtrain = xgb.DMatrix(train_data , label=y_train_data)
    # prepareing params for "Booster parameters"
    params={
        'learning_rate':2,
        'min_split_loss':0.1
    }
    cv_result = xgb.cv(dtrain=dtrain,params=params,nfold=5 ,num_boost_round=100 ,early_stopping_rounds=5 ,verbose_eval=True)
    
    return(cv_result)

