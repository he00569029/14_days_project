# import packages
import pandas as pd
import preprocessing as pre
from sklearn.feature_selection import SelectKBest, chi2, f_classif , SelectFromModel
from sklearn.linear_model import LassoCV, Lasso, LogisticRegressionCV, LogisticRegression

class FS_params():
    def __init__(self , train_data , y_train_data):
        self.train_data = train_data
        self.y_train_data = y_train_data

    # Select K Best
    def fs_select_k_best(self , k_col ):
        selector = SelectKBest(score_func=f_classif , k=k_col)
        x_new = selector.fit_transform(self.train_data , self.y_train_data )
        k_best_train_data = pd.DataFrame(selector.inverse_transform(x_new) , columns=self.train_data.columns ,index=self.train_data.index)
        pre.drop_constant_column(k_best_train_data)

        k_best_data={'k_best_train_data':k_best_train_data, 'k_best_column':k_best_train_data.columns}
        return(k_best_data)


    # L1 regularisation(Lasso)
    def fs_lasso(self):
        selector = SelectFromModel(estimator=LogisticRegression(solver='liblinear',penalty='l1')).fit(self.train_data, self.y_train_data)
        fs_train_data = pd.DataFrame(selector.transform(self.train_data) , columns=self.train_data.columns[selector.get_support()] , index = self.train_data.index)
        fs_feature = list(self.train_data.columns[selector.get_support()])

        fs_data={'fs_train_data':fs_train_data , 'fs_feature':fs_feature}
        return(fs_data)


    # L2 regularisation(Rigde)
    def fs_ridge(self):
        selector = SelectFromModel(estimator=LogisticRegression(penalty='l2')).fit(self.train_data,self.y_train_data)
        fs_train_data = pd.DataFrame(selector.transform(self.train_data) , columns=self.train_data.columns[selector.get_support()] , index = self.train_data.index)
        fs_feature = list(self.train_data.columns[selector.get_support()])

        fs_data={'fs_train_data':fs_train_data , 'fs_feature':fs_feature}
        return(fs_data)
