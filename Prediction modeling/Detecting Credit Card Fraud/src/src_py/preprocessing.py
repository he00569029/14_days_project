import pandas as pd

def drop_constant_column(data):
    constant_column=[]
    for column in data.columns:
        if len(data[column].value_counts())==1 :
            constant_column.append(column)
    return(data.drop(constant_column, axis=1 ,inplace=True))


def deal_missing_column(data):
    missing_column = data.columns[data.isnull().any()==True]
    for column_name in missing_column:
        if data[column_name].dtype == 'int64':
            data[column_name].fillna(data[column_name].mean(), inplace=True)






    
