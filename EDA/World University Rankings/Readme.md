# World University Rankings

## Topic Description

---
Of all the universities in the world, which are the best?

Ranking universities is a difficult, political, and controversial practice. There are hundreds of different national and international university ranking systems, many of which disagree with each other. This dataset contains three global university rankings from very different places.

### University Ranking Data

The Times Higher Education World University Ranking is widely regarded as one of the most influential and widely observed university measures. Founded in the United Kingdom in 2010, it has been criticized for its commercialization and for undermining non-English-instructing institutions.

The Academic Ranking of World Universities, also known as the Shanghai Ranking, is an equally influential ranking. It was founded in China in 2003 and has been criticized for focusing on raw research power and for undermining humanities and quality of instruction.

The Center for World University Rankings, is a less well know listing that comes from Saudi Arabia, it was founded in 2012.

## Goal

- How do these rankings compare to each other?
- Are the various criticisms levied against these rankings fair or not?
- How does your alma mater fare against the world?

## Data Description

- Three global university rankings from very different places.
  - Times Higher Education World University Ranking. (timesData)
  - Academic Ranking of World Universities.(shanghaiData)
  - Center for World University Rankings.(cwurData)
- timesData columns
  - world_rank
  - university_name
  - country
  - teaching : 教學分數 (學習環境)
  - international :國際展望的分數 (職員、學生、研究)
  - research :研究分數 (數量、收入、聲譽)
  - citations :該大學被引用的分數 (研究上的影響力)
  - income
  - total_score : 總分，被用來當作排名的依據
  - num_students
  - student_staff_ratio: 學生人數除以職員人數，學生職員的比例
  - international_students: 國際學生的比例
  - female_male_ratio
  - year:year of the ranking (2011 to 2016 included)
