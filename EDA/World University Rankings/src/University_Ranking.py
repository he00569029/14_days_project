# import packages
import pandas as pd
import seaborn as sns;sns.set()
import matplotlib.pyplot as plt
import numpy as np
# load data
PATH='C:/Users/he00569029/Desktop/vscode_dir/14_days_project/EDA/World University Rankings/Dataset/'
times_data = pd.read_csv(PATH+'timesData.csv')


# Data Description
times_data.info()
times_data.describe(include='all')
times_data.shape

# Missing Value Check
times_data.isnull().any()
# Find the missing value data in the columns
[col for col in times_data.columns if times_data[col].isnull().sum()>0]


# Find constant column
constant_column =[]
for col in times_data.columns:
    if len( times_data[col].value_counts() ) == 1:
        constant_column.append(col)
print("The dataset have",len(constant_column),"column have constant variable, the columns is",constant_column)

# Transform data type to float64
cov_col = ['international','income','total_score','num_students']
times_data['num_students'] = times_data['num_students'].str.replace(',','')
for col in cov_col:
    index = times_data[ times_data[col] == '-'].index
    times_data.loc[index , col] = np.nan
    times_data[col] = pd.to_numeric( times_data[col] )


# pairplot
fig = sns.pairplot(times_data,plot_kws=dict(scatter_kws=dict(s=2)),diag_kind="kde" ,kind='reg')
plt.tight_layout()
plt.show()

# distplot for numeric feature
numeric_col = times_data.select_dtypes(include='float64').columns
fig,axes = plt.subplots(2,4)
sns.distplot(  times_data[numeric_col[0]],  ax=axes[0,0]  )
sns.distplot(  times_data[numeric_col[1]],  ax=axes[0,1]  )
sns.distplot(  times_data[numeric_col[2]],  ax=axes[0,2]  )
sns.distplot(  times_data[numeric_col[3]],  ax=axes[0,3]  )
sns.distplot(  times_data[numeric_col[4]],  ax=axes[1,0]  )
sns.distplot(  times_data[numeric_col[5]],  ax=axes[1,1]  )
sns.distplot(  times_data[numeric_col[6]],  ax=axes[1,2]  )
sns.distplot(  times_data[numeric_col[7]],  ax=axes[1,3]  )
plt.tight_layout()
plt.show()


# boxplot for numeric feature
fig , axes = plt.subplots(2,4)
sns.boxplot(  x=numeric_col[0], data=times_data ,orient='v', ax=axes[0,0]  , fliersize=2)
sns.boxplot(  x=numeric_col[1], data=times_data ,orient='v', ax=axes[0,1]  , fliersize=2)
sns.boxplot(  x=numeric_col[2], data=times_data ,orient='v', ax=axes[0,2]  , fliersize=2)
sns.boxplot(  x=numeric_col[3], data=times_data ,orient='v', ax=axes[0,3]  , fliersize=2)
sns.boxplot(  x=numeric_col[4], data=times_data ,orient='v', ax=axes[1,0]  , fliersize=2)
sns.boxplot(  x=numeric_col[5], data=times_data ,orient='v', ax=axes[1,1]  , fliersize=2)
sns.boxplot(  x=numeric_col[6], data=times_data ,orient='v', ax=axes[1,2]  , fliersize=2)
sns.boxplot(  x=numeric_col[7], data=times_data ,orient='v', ax=axes[1,3]  , fliersize=2)
plt.tight_layout()
plt.show()



# Pearson correlation heatmap 
fig = sns.heatmap(  times_data.corr() , annot=True , fmt='.1f')
plt.tight_layout()
plt.show()

# Spearman correlation heatmap 
fig = sns.heatmap( times_data.corr(method='spearman') , annot=True , fmt='.1f')
plt.tight_layout()
plt.show()




# top 100 university and country countplot
fig = sns.countplot(x='country' , data=times_data[0:100])
fig.set_xticklabels(fig.get_xticklabels() , rotation=45)
plt.tight_layout()
plt.show()
# we found a lot of university is location in USA and UK

# top 20 university and income barplot
fig = sns.barplot(x='income', y='university_name' , data=times_data[0:20] )
fig.set_xticklabels(fig.get_xticklabels() , rotation=45)
plt.tight_layout()
plt.show()
# rank and income are not related

# top 20 university and student_staff_ratio barplot
fig = sns.barplot(x='student_staff_ratio' , y='university_name' , data=times_data[0:20])
plt.tight_layout()
plt.show()
# The ratio of student_staff_ratio in most university is 5~10%
# Fewer university is lower 5% and over 10%


# top 20 university and international_students barplot
times_data['international_students'] = times_data['international_students'].str.replace('%','').to_numeric(times_data['international_students'])
# times_data['international_students'] = pd.to_numeric(times_data['international_students'])
times_data.rename(columns={'international_students':'international_students_ratio'} , inplace=True)
fig = sns.barplot(x='international_students_ratio' , y='university_name' , data=times_data[0:20])
plt.tight_layout()
plt.show()
# The higher international_students_ratio of Imperial College London, ETH Zurich, Carnegie Mellon University

# Scatter pairplot
use_col = ['teaching','international','research','citations','total_score']
fig = sns.pairplot(times_data[use_col] ,plot_kws=dict(scatter_kws=dict(s=2)),diag_kind="kde" ,kind='reg')
plt.tight_layout()
plt.show()
# total_score and teaching have positive correlation
# total_score and research have positive correlation
# research and teaching have positive correlation



