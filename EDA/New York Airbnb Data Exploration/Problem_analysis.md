## New York Airbnb Data Exploration

### Target Question
- Which hosts are the busiest and why?
- What areas have more traffic than others and why is that the case?
- Are there any relationships between prices, number of reviews, and the number of days that a given listing is booked?

### Analysis Step
- Know the data describe and structure
- EDA for question
- Conclusion



##### Know the data describe
- We know this dataset have 48895 rows and 16 columns
- 3 columns of float64 type, 7 columns of int64 type, 6 columns of object type
- In name、host_name、last_review、reviews_permonth those columns have missing value

##### EDA for pairplot
- we want to know which houses will get popular, so we use some of features to observed that these maybe affect each other
>_Because use all columns to plot fig lead this fig too big, so we decide focus on few column, like price、number_of_reviews、availability_365、minimum_nights_
- We assume lower price will get more reviews. Everyone want to live in a house with higer price-performance ratio, so lower price can attract more people to live. As more people live here, there will be more reviews.
- We assume the more popular house will get lower availability_365 value. Because everyone want to live in here, so those house will be booked early.
- The result shows two intersting things.
  - lower price will get more reviews
  - higher price will get lower minimum_nights
##### EDA for question
---
###### Which hosts are the busiest and why?
> For this question we have to clear denfined 'busiest host'
> We Assume three case
> - Host owner most house
> - These house in difference areas
> - Host owner most house and these house cross difference areas
  - workflow step
    - assume one
      - defined busiest host, who have owner over 100 house
        >The results show many hosts have owner over 100 house
    - assume two
      - definded busiest host, who have owner house cross difference areas
        >The results show many hosts have owner house cross difference areas
    - assume three
      - definded busiest host, who have owner over 300 ~ 600 house and cross differenc areas
        >The results show few host have owner over 500 house and cross 5 areas
>_Finally, we find the busiest host, who is Michael_
---
###### What areas have more traffic than others and why is that the case?
>For this question we have to defined 'which areas have most traffic'?
>- We assume that the area with more houses is more prosperous or convenient transportation.
>- We assume that area with some houses of higher price, because prosperous area don't price-cutting competition.
>- We assume that area have more popular tourist attraction.
- Workflow step
  - Know every area price per room
  - Find which area have most house
  - Combine those condition
- Conclusion
  - For the first hypothesis, we found that Brooklyn and Manhattan have more houses.
  - For the second hypothesis, we know both Brooklyn and Manhattan have houses of higher price.
  - For the third hypothesis, most popular tourist attraction is in the Manhattan.
---
###### Are there any relationships between prices, number of reviews, and the number of days that a given listing is booked?
>- Price & number of reviews
>- Price & availability_365
>- Number of reviews & availability_365
- For these feature, use scatter or pairplot to observe these are related
- Price & number of reviews
  - We assume the lower price will get more number of reviews. Because lower price will attract more people to rent house.
  - If more people rented a house, number of reviews easily get ascent. Otherwise, the higher price will get less number of reviews.  Because higher price doesn't attract people to rent house. If few people rented a house, number of reviews easily get decrease.
  - In fact, in this pairplot, lower price have more reviews, casue price can attracted people to rent these house.And, higher price can't attract people to rent these house. As the price is higher, there will be fewer reviews.
- Price & availability_365
  - We assume higher price will get higher availability_365. 
  - Although room of higher price can enjoy higher quailty, but it means you should pay a lot of rent. Tenant must think a long time, lead to dare not to rent. Eventually, higher price of house will get higher availability_365.
  - Otherwise, lower price will get lower availability_365. Because everyone want to rent a higher house of price-performance ratio. For this reason, house of lower price have lower availability_365.
  - In fact, house of higher price have higher availability_365 or lower availability_365, it's irrelevant to price. And, house of lower price have include all range of 365 days. It's irrelevant to availability_365.

- Number of reviews & availability_365
  - We assume more reviews of rooms most likely better room, so everyone want to book these rooms. For this reason, lead to get less number of days of availability_365 of better room.
  - But, in this pairplot, we observed number of reviews and availability_365 haven't any relationship. Because more reviews still have higher number of days of availability_365. 
  - So, we infer number of days of availability_365 is not affected by number of reviews.