## New York Airbnb Data Exploration
---
### Target Question
- Which hosts are the busiest and why?
- What areas have more traffic than others and why is that the case?
- Are there any relationships between prices, number of reviews, and the number of days that a given listing is booked?




### Data Description
- id    :id
- name  :名稱
- host_id  :房東ID
- host_name :房東名稱
- neighbourhood_group : 房子所在的區域名稱  因為都在紐約市  而紐約市分為5大行政區  因此此欄位階為顯示房子所在的行政區是哪一個
- neighbourhood  : 行政區內所在的街區  
- latitude  :緯度
- longitude :經度
- room_type :房間形態
- price :價格
- minimum_nights  : 最少的入住天數
- number_of_reviews :有多少人評論
- last_review  :最近評論的日期
- review_per_month  :平均每個月評論多少次數
- calculated_host_listings_count  : 房東會切幾間房間去分別刊登在airbnb
- availability_365  : 365天內這間房間  剩下幾天可以預定


### Links
- [Kaggle Airbnb](https://www.kaggle.com/dgomonov/new-york-city-airbnb-open-data)
- [Dataset Acknowledgements](http://insideairbnb.com/)

