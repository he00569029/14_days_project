## Most Important Factors related to Employee Attrition and Performance
---

>_再思考問題前，必須先了解「Attrition」是甚麼意思
>Attrition是指公司內部的離職率_

#### Reasons of employee attrition
- Work overtime
- Work environment too bad
- Bad management
- A better work opportunities
  
### Question
- Does really many employee want to attrition?
- Which difference between attrition and non-attrition?
- Reasons of attrition
  
### Analysis Step
- Know the data structure
  - About how many people and columns in this dataset
  - Does any missing value?
  - Does any constant variable?
  - which variable is output?
- Exploratory Data Analysis




### Know the data structure
- This dataset have 1470 row and 35 columns
- 26 columns type is int64 and remain 9 columns type is object
- This dataset have no missing value
- Output feature is "Attrition". Distribution of output. We found that the "Attrition" feature is imbalance, most employee doesn't quite the organization.


### Exploratory Data Analysis
- Is age or gender related to employee attrition? Are there any significant discrepancies?.
  - we can observed that over the 35 years old not so many attrition in age countplot.
  - we also observed that male have attrition more than female. 
- Is job environment effect attrition?
  - we observed that even job environment is great still have people have attrition, it's almost the same amount as the environment level 1.
  - But environment level 1 is a group with highest proportion.  
- Is overtime related to employee attrition?
  - we observed that work overtime is almost the same amount as didn't overtime.
- Relationship Satisfaction could related to employee attrition?
  - we observed that relationship satisfaction level 1 to 4 does not affect attrition.
- Is job level related to attrition?
  - We found Job Level 1 is the highest proportion of attrition
- Is department related to attrition?
  - Alought research have most attrition number, but attrition ratio of sales is higher.
- Is maritalStatus related to attrition?
  - We found that Signle is the highest proportion of attrition.
- Is PercentSalaryHike related to attrition?
  - After salaryhike more than 14%, the proportion of attrition is less.
- Is DistanceFromHome related to attrition?
  - The closer your company, the more you will attrtion
- Is DistanceFromHome and other feature related to attrition?
  - We found that DistanceFromHome over 10 km and Married is the highest proportion of attrition.
  - We found that DistanceFromHome lower 5 km and JobLevel 5  will be attrition.
  - We found that DistanceFromHome over 10 km and Female is higher proportion of attrition.
- Correlation heatmap for numeric (Pearson and Spearman)
  - Pearson correlation
    - Feature with correlation coefficient is between 0.7 and 0.8
      - TotalWorkingYears & Age
      - YearsInCurrentRole & YearsWithCurrManager
    - Feature with correlation coefficient is between 0.8 and 0.9
      - JobLevel & TotalWorkingYears
      - MonthlyIncome & TotalWorkingYears
      - YearsAtCompany & YearsWithCurrManager
      - YearsAtCompany & YearsInCurrentRole
      - PercentSalaryHike & PerformanceRating
  - Spearman correlation
    - Feature with correlation cofficient is between 0.7 and 0.8
      - TotalWorkingYears & Age
      - JobLevel & TotalWorkingYears
      - MonthlyIncome & TotalWorkingYears
      - YearsInCurrentRole & YearsWithCurrManager
    - Feature with correlation cofficient is between 0.8 and 0.9
      - YearsAtCompany & YearsWithCurrManager
    - Feature with correlation cofficient is between 0.9 and 1
      - YearsAtCompany & YearsInCurrentRole
- Pairplot
