import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns;sns.set()
import markdown
import load_data
import preprocessing
from scipy import stats
# load data
PATH = "C:/Users/he00569029/Desktop/vscode_work_dictionary/Practice_project/EDA_project/Most_important_factors_related_to_employee_attrition_and_performance/Dataset/"
data_name='WA_Fn-UseC_-HR-Employee-Attrition'
employee = load_data.load_data(PATH,data_name)

# Data describle
employee.shape
employee.head()
employee.columns
employee.describe(include='all')
employee.info()

# Chech missing value
employee.isnull().any()
# Find the missing value data in the columns
[col for col in employee.columns if employee[col].isnull().sum()>0]

# Find constant column
constant_column =[]
for col in employee.columns:
    if len( employee[col].value_counts() ) == 1:
        constant_column.append(col)
print("The dataset have",len(constant_column),"column have constant variable, the columns is",constant_column)



# Attrition feature Countplot
Attrition_countplot = sns.countplot(x='Attrition',data=employee)
plt.tight_layout()
plt.show(Attrition_countplot)


## EDA
# Age countplot
age_countplot = sns.countplot(x='Age' , hue='Attrition', data=employee)
plt.tight_layout()
plt.show(age_countplot)

# Gender coutplot
gender_countplot = sns.countplot( x='Gender' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(gender_countplot)
# We found Male is the highest proportion of attrition

# EnvironmentSatisfaction countplot
env_sat_countplot = sns.countplot( x='EnvironmentSatisfaction' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(env_sat_countplot)
# We found Environment Satisfaction 1 is the highest proportion of attrition

# Overtime countplot
overtime_countplot = sns.countplot( x='OverTime' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(overtime_countplot)

# RelationshipSatisfaction countplot
relation_sat_countplot = sns.countplot( x='RelationshipSatisfaction' , hue='Attrition', data=employee)
plt.tight_layout()
plt.show(relation_sat_countplot)

# Job level countplot
joblevel_countplot = sns.countplot( x='JobLevel', hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(joblevel_countplot)
# We found Job Level 1 is the highest proportion of attrition

# Department countplot
department_countplot = sns.countplot( x='Department' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(department_countplot)

# MaritalStatus countplot
marry_countplot = sns.countplot( x='MaritalStatus' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(marry_countplot)
# We found that Signle is the highest proportion of attrition

# PercentSalaryHike countplot
salaryhike_countplot = sns.countplot(x='PercentSalaryHike' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(salaryhike_countplot)
# After salaryhike more than 14%, the proportion of attrition is less

# DistanceFromHome countplot
distance_countplot = sns.countplot(x='DistanceFromHome' , hue='Attrition' , data=employee)
plt.tight_layout()
plt.show(distance_countplot)
# The closer your company, the more you will attrtion

# distancefromhome subplot barplot
fig,axs=plt.subplots(2,2 , figsize=(12,8))
sns.barplot(x='Attrition' , y='DistanceFromHome' ,hue='MaritalStatus', data=employee ,ax=axs[0][0])
sns.barplot(x='Attrition' , y='DistanceFromHome' ,hue='Gender', data=employee ,ax=axs[0][1])
sns.barplot(x='Attrition' , y='DistanceFromHome' ,hue='JobLevel', data=employee ,ax=axs[1][0])
sns.barplot(x='Attrition' , y='DistanceFromHome' ,hue='EnvironmentSatisfaction', data=employee ,ax=axs[1][1])
plt.tight_layout()
plt.show()


# correlation heatmap for numeric feature
fig,ax = plt.subplots(figsize=(12,8))
fig = sns.heatmap(employee.corr() , annot=True , fmt='.1f' , linewidths=.5 , ax=ax , cmap='coolwarm')
plt.title('Pearson Correlation Heatmap', fontsize=20)
plt.tight_layout()
plt.show(fig)

# spearman correlation heatmap for numeric feature
fig,ax = plt.subplots(figsize=(12,8))
fig = sns.heatmap(employee.corr(method='spearman') , annot=True , fmt='.1f' , linewidths=.5 , ax=ax , cmap='coolwarm')
plt.title('Spearman Correlation Heatmap', fontsize=20)
plt.tight_layout()
plt.show(fig)



# pairplot
using_column = ['Age','YearsSinceLastPromotion','DistanceFromHome','PercentSalaryHike','MonthlyRate','Attrition','JobLevel','TotalWorkingYears']
employee_pairplot = sns.pairplot(employee[using_column]  , plot_kws=dict(scatter_kws=dict(s=2)) , hue='Attrition' ,diag_kind="kde" ,kind='reg')
plt.tight_layout()
plt.show(employee_pairplot)



